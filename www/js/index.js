angular.element(document).ready(function () {
    if (window.cordova) {
        document.addEventListener('deviceready', function () {
            angular.bootstrap(document.body, ['app']);
        }, false);
    } else {
        angular.bootstrap(document.body, ['app']);
    }
});


angular.module('app',['onsen'])
    //write your controller here